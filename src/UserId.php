<?php

namespace WPDesk\Nps;

/**
 * Provides user ID.
 */
interface UserId {

    /**
     * @return string
     */
    public function get_id();

}
