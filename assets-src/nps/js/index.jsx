import WPDeskNPS from "./components/wpdesknps";
import React from 'react';
import ReactDOM from 'react-dom';

document.addEventListener( 'DOMContentLoaded', function () {
	let element = document.getElementById('wpdesk-nps');
	if ( element ) {
		ReactDOM.render(
			<WPDeskNPS
                npsId={element.getAttribute('npsId')}
				worstLabel={element.getAttribute('worstLabel')}
				bestLabel={element.getAttribute('bestLabel')}
				question={element.getAttribute('question')}
				feedbackQuestion={element.getAttribute('feedbackQuestion')}
                feedbackOptions={JSON.parse(element.getAttribute('feedbackOptions'))}
				disclaimer={element.getAttribute('disclaimer')}
				sendButton={element.getAttribute('sendButton')}
				sendButtonSending={element.getAttribute('sendButtonSending')}
				writeKey={element.getAttribute('writeKey')}
				campaignId={element.getAttribute('campaignId')}
				feedbackId={element.getAttribute('feedbackId')}
				thankYouMessage={element.getAttribute('thankYouMessage')}
				userId={element.getAttribute('userId')}
                ajaxUrl={element.getAttribute('ajaxUrl')}
			/>,
			element
		);
	}
});
