import React, {Component} from 'react';

const MIN = 0
const MAX = 10

export default class NPSScore extends Component {

	/**
	 * @param {Object} props
	 */
	constructor( props ) {

		super(props);

		this.state = {
			score: props.score || null,
			selected: null,
			worstLabel: props.worstLabel || `Not at all likely`,
			bestLabel: props.bestLabel || `Extremely likely`,
			onSubmit: props.onSubmit || null,
		};

		this.range = this.range.bind( this );
		this.handleMouseEnter = this.handleMouseEnter.bind( this );
		this.handleMouseLeave = this.handleMouseLeave.bind( this );
		this.handleClick = this.handleClick.bind( this );
	}

	range( start, end ) {
		return Array.from({ length: end - start + 1 }).map((_, idx) => start + idx)
	}

	handleMouseEnter( score ) {
		this.setState( { score: score } );
	}

	handleMouseLeave() {
		this.setState( { score: null } )
	}

	handleClick( score ) {
		this.setState( { score: score, selected: score } );
		if ( this.state.onSubmit ) {
			this.state.onSubmit( score );
		}
	}

	render() {
		let value = this.state.score || this.state.selected;
		return (
			<div className={"nps-scale"}>
				<div>
					{this.range(MIN, MAX).map((i) => (
						<div
							key={i}
							className={`${"value"} ${
								value !== null && value >= i ? "selected" : ''
							}`}
							onMouseEnter={() => this.handleMouseEnter(i)}
							onMouseLeave={this.handleMouseLeave}
							onClick={() => this.handleClick(i)}
						>
							<div>{i}</div>
						</div>
					))}
				</div>
				<div className={"legend"}>
					<div className={`${"label"} ${"left"}`}>{this.state.worstLabel}</div>
					<div className={`${"label"} ${"right"}`}>{this.state.bestLabel}</div>
				</div>
			</div>
		)
	}
}
