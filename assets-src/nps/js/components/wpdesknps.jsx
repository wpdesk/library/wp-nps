import {Component} from "react";
import NPSScale from "./NPSScale";
import React from 'react';
import Parser from "html-react-parser";
import { __, _x, _n, _nx, sprintf } from "@wordpress/i18n";
import FeedbackOptions from "./FeedbackOptions";

export default class WPDeskNPS extends Component {

	/**
	 * @param {Object} props
	 */
	constructor( props ) {
		super(props);
		this.state = {
			dismissed: false,
			score: null,
			how_to_improve: '',
			sending: false,
			sent: false,
			error: false,
            selected_option: '',
            checked_option: '',
            textarea_value: '',
		};

		this.dismiss = this.dismiss.bind( this );
		this.sent = this.sent.bind( this );
		this.set_dismissed = this.set_dismissed.bind( this );
		this.set_score = this.set_score.bind( this )
		this.send_nps = this.send_nps.bind( this );
		this.set_how_to_improve = this.set_how_to_improve.bind( this );
		this.set_checked_option = this.set_checked_option.bind( this );
		this.set_textarea_value = this.set_textarea_value.bind( this );
        this.set_how_to_improve_from_option = this.set_how_to_improve_from_option.bind( this );
	}

	sent() {
		this.setState( { sent: true } );
		this.send_ajax( 'sent' );
	}

    /**
     * @param {string} nps_action
     */
	send_ajax( nps_action ) {
        const formData = new FormData();
        formData.append( 'checked_option_id', this.state.checked_option.id );

        const requestOptions = {
            method: 'POST',
            body: formData
        };

        fetch(this.props.ajaxUrl + '&action=wpdesk_nps_' + nps_action, requestOptions)
            .then(response => response.json())
        ;
    }

	dismiss() {
	    this.send_ajax( 'dismiss' );
		this.set_dismissed( true );
	}

    /**
     * @param {boolean} dismissed
     */
	set_dismissed( dismissed ) {
		this.setState( { dismissed: dismissed } )
	}

    /**
     * @param {string} score
     */
	set_score( score ) {
		this.setState( { score: score } );
	}

    /**
     * @param {Event} event
     */
	set_how_to_improve( event ) {
		this.setState( { how_to_improve: event.target.value } )
	}

    /**
     * @param {Object} checked_option
     */
	set_checked_option( checked_option ) {
		this.setState( { checked_option: checked_option } )
	}

    /**
     * @param {String} textarea_value
     */
    set_textarea_value( textarea_value ) {
		this.setState( { textarea_value: textarea_value } )
	}

    /**
     * @param {string} option_id
     * @param {string} how_to_improve
     */
    set_how_to_improve_from_option( option_id, how_to_improve ) {
        this.setState( { selected_option: option_id, how_to_improve: how_to_improve } )
    }

	send_nps( event ) {
		let self = this;

        this.setState( { error: false } );

        if (this.state.checked_option.required_additional_question && this.state.textarea_value.length === 0) {
            this.setState({error: true});

            return;
        }

        this.setState( { sending: true } );

		const requestOptions = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify( {
				"writeKey": this.props.writeKey,
				"campaign": this.props.campaignId,
				"method": "In-App",
				"rating": this.state.score,
				"answers": [
					{
						"id": this.props.feedbackId,
						"value": this.state.how_to_improve,
					}
				],
				"userId": this.props.userId,
			} )
		};
		fetch('https://app.satismeter.com/api/responses', requestOptions)
			.then( response => response.json() ).finally(() => { self.sent() } ).catch( () => {} )
		;
	}

	render() {
		return this.state.dismissed ? null : (
			<div className={"wpdesk-nps"}>
				<div className={"root animated"}>
					<button className="close" onClick={this.dismiss}>
						✕
					</button>
					{!this.state.sent &&
						<div className="inner">
							<p className={"message"}>{this.props.question}</p>
							<NPSScale
								worstLabel={this.props.worstLabel}
								bestLabel={this.props.bestLabel}
								score={this.state.score}
								onSubmit={(score) => {
									this.set_score(score)
								}}
							>
							</NPSScale>
							{this.state.score !== null &&
								<div>
									<div className="feedback">
										<label>{this.props.feedbackQuestion || "What could we do to improve?"}</label>
                                        { ! this.props.feedbackOptions.length &&
                                            <textarea onChange={this.set_how_to_improve}/>
                                        }
                                        { this.props.feedbackOptions.length &&
                                            <FeedbackOptions
                                                feedback_options={this.props.feedbackOptions}
                                                required_error={this.state.error}
                                                required_field_message={this.props.requiredFieldMessage}
                                                change_how_to_imporove={this.set_how_to_improve_from_option}
                                                set_checked_option={this.set_checked_option}
                                                set_textarea_value={this.set_textarea_value}
                                            />
                                        }
									</div>
									<div className={"send"}>
										<span>{Parser( this.props.disclaimer )}</span>
										<button
											disabled={this.state.sending}
											onClick={this.send_nps}
										>{this.state.sending ? this.props.sendButtonSending || "Sending" : this.props.sendButton || "Send" }</button>
									</div>
								</div>
							}
						</div>
					}

					{this.state.sent &&
						<div className="inner">
							<p className={"message"}>{this.props.thankYouMessage || "Thank you!"}</p>
						</div>
					}
				</div>
			</div>
		)
	}
}
