import {Component} from "react";
import React from 'react';

export default class FeedbackOptions extends Component {

	/**
	 * @param {Object} props
	 */
	constructor( props ) {
		super(props);
		this.state = {
		    checked: -1,
            textarea_value: ""
        };

		this.set_textarea_value = this.set_textarea_value.bind( this );
        this.set_checked = this.set_checked.bind( this );
	}

    /**
     * @param {Number} checked
     * @param {String} textarea_value
     */
	set_how_to_improve( checked, textarea_value ) {
	    let how_to_improve = this.props.feedback_options[ checked ].label;
	    if ( textarea_value.length !== 0) {
	        how_to_improve = how_to_improve + ": " + textarea_value;
        }

		this.props.change_how_to_imporove( this.props.id, how_to_improve );
	}

    /**
     * @param {Event} event
     */
    set_checked( event ) {
        let checked = parseInt( event.target.value );
        this.setState( { checked: checked } );
        this.set_how_to_improve( checked, this.state.textarea_value );

        this.props.set_checked_option( this.props.feedback_options[ checked ] );
    }

    /**
     * @param {Event} event
     */
    set_textarea_value( event ) {
        this.setState( { textarea_value: event.target.value } );
        this.set_how_to_improve( this.state.checked, event.target.value );
        this.props.set_textarea_value( event.target.value );
    }

    /**
     * @return {boolean}
     */
    has_additional_question() {
        if ( this.state.checked >= 0 ) {
            let option = this.props.feedback_options[ this.state.checked ];
            return option.additional_question !== "";
        }

        return false;
    }

    /**
     * @return {String}
     */
    get_additional_question() {
        let option = this.props.feedback_options[ this.state.checked ];
        return option.additional_question;
    }

    /**
     * @return {String}
     */
    is_required_additional_question() {
        let option = this.props.feedback_options[ this.state.checked ];
        return option.required_additional_question;
    }

    render() {
        let self = this;
	    return (
            <div className={"feedback-options"}>
                {this.props.feedback_options.map((option, i) => {
                    let id = "feedback-option-" + i;
                    return (
                        <div className={"feedback-option"}>
                            <input
                                id={id}
                                type="radio"
                                name='wpdesk-nps-feedback-option'
                                checked={self.state.checked === i}
                                value={i}
                                onChange={self.set_checked}
                            /><label for={id}>{option.label}</label>
                        </div>
                    )
                })}
                { this.has_additional_question() &&
                    <div className={"feedback-question"}>
                        <label>{this.get_additional_question()}</label>
                        <textarea required={this.is_required_additional_question()} autoFocus={true} onChange={this.set_textarea_value}>{this.state.textarea_value}</textarea>

                        {this.props.required_error &&
                            <div className="required-field-error">
                                {this.props.requiredFieldMessage || "This field is required"}
                            </div>
                        }

                    </div>
                }
            </div>
       )
	}
}
