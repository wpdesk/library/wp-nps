## [1.3.0] - 2022-07-13
### Added
- Support for user_suffix in UuidUserId class
- advanced user meta editing
- action for nps/sent
- support for required answers

## [1.2.5] - 2022-07-04
### Fixed
- Return type of FeedbackOption::jsonSerialize() should either be compatible with JsonSerializable::jsonSerialize(): mixed

## [1.2.4] - 2021-12-07
### Fixed
- translation

## [1.2.3] - 2021-09-29
### Fixed
- allowed wp-builder v 2

## [1.2.2] - 2021-08-30
### Changed
- nps z-index
### Fixed
- question text

## [1.2.1] - 2021-08-30
### Changed
- hidden radio input 

## [1.2.0] - 2021-08-30
### Added
- improved predefined answers

## [1.1.0] - 2021-08-26
### Added
- predefined answers

## [1.0.0] - 2021-06-01
### Added
- init
