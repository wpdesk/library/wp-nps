<?php

namespace unit;

use WP_Mock\Tools\TestCase;
use WPDesk\Nps\AjaxHandler;
use WPDesk\Nps\Assets;
use WPDesk\Nps\DisplayDecisions\DisplayNpsDecision;
use WPDesk\Nps\Element;
use WPDesk\Nps\FeedbackOption;
use WPDesk\Nps\UserId;

class ElementTest extends TestCase {

    /**
     * @var \PHPUnit\Framework\MockObject\MockObject|DisplayNpsDecision
     */
    private $display_nps_decisions;
    /**
     * @var Element
     */
    private $element_under_tests;

    public function setUp() {
        \WP_Mock::setUp();

        $user_id_provider = $this->getMockBuilder( UserId::class )->setMethods( array( 'get_id' ) )->getMock();
        $user_id_provider->method( 'get_id' )->willReturn( 'user_id' );

        $this->display_nps_decisions = $this->getMockBuilder( DisplayNpsDecision::class )->setMethods( array( 'should_display_nps' ) )->getMock();
        $this->element_under_tests = new Element(
            'nps_id',
            $this->display_nps_decisions,
            'element_id',
            'worst_label',
            'best_label',
            'question',
            'feedback_question',
            [ new FeedbackOption( 'feedback_option', 'question' ) ],
            'disclaimer',
            'send_button',
            'send_button_sending',
            'write_key',
            'campaign_id',
            'feedback_id',
            'thank_you_message',
            $user_id_provider,
            'ajax_url'
        );
    }

    public function tearDown() {
        \WP_Mock::tearDown();
    }

    public function testShouldAddHooks() {
        // Expects
        \WP_Mock::expectActionAdded(  'admin_footer', array( $this->element_under_tests, 'display_nps_element' ) );

        // When
        $this->element_under_tests->hooks();

        // Then
        $this->assertTrue( true );
    }

    public function testShouldNotDisplayElementWhenDecisionReturnFalse() {
        // Expects
        $this->expectOutputString( '' );

        $this->display_nps_decisions->method( 'should_display_nps' )->willReturn( false );

        // When
        $this->element_under_tests->display_nps_element();

        // Then
        $this->assertTrue( true );
    }

    public function testShouldDisplayElementWhenDecisionReturnTrue() {
        // Expects
        \WP_Mock::userFunction( 'wp_create_nonce' )->andReturn( 'nonce' );
        $this->expectOutputString( <<<ELEMENT
<div
    id="element_id"
    worstLabel="worst_label"
    bestLabel="best_label"
    question="question"
    feedbackQuestion="feedback_question"
    feedbackOptions="[{"label":"feedback_option","additional_question":"question"}]"
    disclaimer="disclaimer"
    sendButton="send_button"
    sendButtonSending="send_button_sending"
    writeKey="write_key"
    campaignId="campaign_id"
    feedbackId="feedback_id"
    thankYouMessage="thank_you_message"
    userId="user_id"
    ajaxUrl="ajax_url?security=nonce"
></div>

ELEMENT
        );

        $this->display_nps_decisions->method( 'should_display_nps' )->willReturn( true );

        // When
        $this->element_under_tests->display_nps_element();

        // Then
        $this->assertTrue( true );
    }

}
