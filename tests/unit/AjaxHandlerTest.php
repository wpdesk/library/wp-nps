<?php

namespace unit;

use WP_Mock\Tools\TestCase;
use WPDesk\Nps\AjaxHandler;

class AjaxHandlerTest extends TestCase {

    /**
     * @var AjaxHandlerTest
     */
    private $ajax_handler_under_tests;

    public function setUp() {
        \WP_Mock::setUp();

        $this->ajax_handler_under_tests = new AjaxHandler( 'test' );
    }

    public function tearDown() {
        \WP_Mock::tearDown();
    }

    public function testShouldAddHooks() {
        // Expects
        \WP_Mock::expectActionAdded( 'wp_ajax_wpdesk_nps_dismiss', array( $this->ajax_handler_under_tests, 'handle_ajax_dismiss' ) );
        \WP_Mock::expectActionAdded( 'wp_ajax_wpdesk_nps_sent', array( $this->ajax_handler_under_tests, 'handle_ajax_sent' ) );

        // When
        $this->ajax_handler_under_tests->hooks();

        // Then
        $this->assertTrue( true );
    }

    public function testShouldHandleDismiss() {
        // Expects
        $user = new \stdClass();
        $user->ID = 100;
        \WP_Mock::userFunction( 'check_ajax_referer' )->once()->andReturn( true );
        \WP_Mock::userFunction( 'wp_get_current_user' )->once()->andReturn( $user );
        \WP_Mock::userFunction( 'get_user_meta' )->once()->andReturn( array() );
        \WP_Mock::userFunction( 'current_time' )->once()->andReturn( 1 );
        \WP_Mock::userFunction( 'update_user_meta' )->once()->withArgs( array( 100, 'test', array( 'dismiss_count' => 1, 'dismiss_time' => 1 ) ) )->andReturn( true );
        // When
        $this->ajax_handler_under_tests->handle_ajax_dismiss();

        // Then
        $this->assertTrue( true );
    }

    public function testShouldHandleSent() {
        // Expects
        $user = new \stdClass();
        $user->ID = 100;
        \WP_Mock::userFunction( 'check_ajax_referer' )->once()->andReturn( true );
        \WP_Mock::userFunction( 'wp_get_current_user' )->once()->andReturn( $user );
        \WP_Mock::userFunction( 'get_user_meta' )->once()->andReturn( array() );
        \WP_Mock::userFunction( 'update_user_meta' )->once()->withArgs( array( 100, 'test', array( 'sent' => 'yes' ) ) )->andReturn( true );
        // When
        $this->ajax_handler_under_tests->handle_ajax_sent();

        // Then
        $this->assertTrue( true );
    }

}
