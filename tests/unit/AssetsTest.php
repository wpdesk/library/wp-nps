<?php

namespace unit;

use WP_Mock\Tools\TestCase;
use WPDesk\Nps\AjaxHandler;
use WPDesk\Nps\Assets;
use WPDesk\Nps\DisplayDecisions\DisplayNpsDecision;

class AssetsTest extends TestCase {

    /**
     * @var Assets
     */
    private $assets_under_tests;
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject|DisplayNpsDecision
     */
    private $display_nps_decisions;

    public function setUp() {
        \WP_Mock::setUp();

        $this->display_nps_decisions = $this->getMockBuilder( DisplayNpsDecision::class )->setMethods( array( 'should_display_nps' ) )->getMock();
        $this->assets_under_tests = new Assets( 'url', '1', $this->display_nps_decisions );
    }

    public function tearDown() {
        \WP_Mock::tearDown();
    }

    public function testShouldAddHooks() {
        // Expects
        \WP_Mock::expectActionAdded( 'admin_enqueue_scripts', array( $this->assets_under_tests, 'enqueue_nps_scripts' ) );

        // When
        $this->assets_under_tests->hooks();

        // Then
        $this->assertTrue( true );
    }

    public function testShouldNotEnqueueScriptsWhenDecisionReturnFalse() {
        // Expects
        \WP_Mock::userFunction( 'wp_register_script' )->never();

        $this->display_nps_decisions->method( 'should_display_nps' )->willReturn( false );

        // When
        $this->assets_under_tests->enqueue_nps_scripts();

        // Then
        $this->assertTrue( true );
    }

    public function testShouldEnqueueScriptsWhenDecisionReturnTrue() {
        // Expects
        \WP_Mock::userFunction( 'wp_register_script' )->once();
        \WP_Mock::userFunction( 'wp_enqueue_script' )->once();
        \WP_Mock::userFunction( 'wp_enqueue_style' )->once();
        \WP_Mock::userFunction( 'trailingslashit' )->andReturnArg( 0 );

        $this->display_nps_decisions->method( 'should_display_nps' )->willReturn( true );

        // When
        $this->assets_under_tests->enqueue_nps_scripts();

        // Then
        $this->assertTrue( true );
    }

}
