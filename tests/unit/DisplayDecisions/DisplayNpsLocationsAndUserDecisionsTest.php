<?php

namespace unit\DisplayDecisions;

use WP_Mock\Tools\TestCase;
use WPDesk\Nps\DisplayDecisions\AdminPageDisplayNpsDecision;
use WPDesk\Nps\DisplayDecisions\DisplayNpsLocationsAndUserDecisions;
use WPDesk\Nps\DisplayDecisions\UserDisplayNpsDecision;

class DisplayNpsLocationsAndUserDecisionsTest extends TestCase {

    public function setUp() {
        \WP_Mock::setUp();
        if ( ! defined( 'WEEK_IN_SECONDS' ) ) {
            define( 'WEEK_IN_SECONDS', 60 * 60 * 24 * 7 );
        }
    }

    public function tearDown() {
        \WP_Mock::tearDown();
    }

    public function testShouldReturnFalseWhenLocationNotMatch() {
        // Given
        $location_display_decisions = new AdminPageDisplayNpsDecision( array( 'page' => 'test' ) );
        $user_display_decisions = new UserDisplayNpsDecision( 'test' );
        $display_nps_locations_and_user_decisions_under_tests = new DisplayNpsLocationsAndUserDecisions(
            array( $location_display_decisions ),
            array( $user_display_decisions )
        );
        $_GET['page'] = 'test2';

        // When
        $actual = $display_nps_locations_and_user_decisions_under_tests->should_display_nps();

        // Then
        $this->assertFalse( $actual );
    }

    public function testShouldReturnFalseWhenLocationMatchAndUserNotMatch() {
        // Expects
        $user = new \stdClass();
        $user->ID = 100;
        \WP_Mock::userFunction( 'wp_get_current_user' )->once()->andReturn( $user );
        \WP_Mock::userFunction( 'get_user_meta' )->once()->andReturn( array( 'sent' => 'yes' ) );

        // Given
        $location_display_decisions = new AdminPageDisplayNpsDecision( array( 'page' => 'test' ) );
        $user_display_decisions = new UserDisplayNpsDecision( 'test' );
        $display_nps_locations_and_user_decisions_under_tests = new DisplayNpsLocationsAndUserDecisions(
            array( $location_display_decisions ),
            array( $user_display_decisions )
        );
        $_GET['page'] = 'test';

        // When
        $actual = $display_nps_locations_and_user_decisions_under_tests->should_display_nps();

        // Then
        $this->assertFalse( $actual );
    }

    public function testShouldReturnTruehenLocationMatchAndUserMatch() {
        // Expects
        $user = new \stdClass();
        $user->ID = 100;
        \WP_Mock::userFunction( 'wp_get_current_user' )->once()->andReturn( $user );
        \WP_Mock::userFunction( 'get_user_meta' )->once()->andReturn( array() );

        // Given
        $location_display_decisions = new AdminPageDisplayNpsDecision( array( 'page' => 'test' ) );
        $user_display_decisions = new UserDisplayNpsDecision( 'test' );
        $display_nps_locations_and_user_decisions_under_tests = new DisplayNpsLocationsAndUserDecisions(
            array( $location_display_decisions ),
            array( $user_display_decisions )
        );
        $_GET['page'] = 'test';

        // When
        $actual = $display_nps_locations_and_user_decisions_under_tests->should_display_nps();

        // Then
        $this->assertTrue( $actual );
    }

}
