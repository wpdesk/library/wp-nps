<?php

namespace unit\DisplayDecisions;

use WP_Mock\Tools\TestCase;
use WPDesk\Nps\DisplayDecisions\ShippingMethodDisplayNpsDecision;

class ShippingMethodDisplayNpsDecisionTest extends TestCase {

    public function setUp() {
        \WP_Mock::setUp();
    }

    public function tearDown() {
        \WP_Mock::tearDown();
    }

    public function testShouldReturnFalseWhenNotOnShippingMethodSettingsMatch() {
        // Given
        $wc_shipping_zones = $this->getMockBuilder( '\WC_Shipping_Zones' )->getMock();
        $shipping_method_display_nps_decision_under_tests = new ShippingMethodDisplayNpsDecision( 'flexible_shipping', $wc_shipping_zones );
        $_GET['page'] = 'test2';

        // When
        $actual = $shipping_method_display_nps_decision_under_tests->should_display_nps();

        // Then
        $this->assertFalse( $actual );
    }

    public function testShouldReturnFalseWhenOnShippingMethodSettingsButMethodIdNotMatch() {
        // Given
        $shipping_method = $this->getMockBuilder( '\WC_Shipping_Method' )->getMock();
        $shipping_method->id = 'flat_rate';
        $shipping_method_display_nps_decision_under_tests = $this->getMockBuilder( ShippingMethodDisplayNpsDecision::class )
                                                                 ->setMethods( array( 'get_shipping_method' ) )
                                                                 ->setConstructorArgs( array( 'flexible_shipping' ) )
                                                                 ->getMock();
        $shipping_method_display_nps_decision_under_tests->method( 'get_shipping_method' )->willReturn( $shipping_method );
        $_GET['page'] = 'wc-settings';
        $_GET['tab'] = 'shipping';
        $_GET['instance_id'] = 1;

        // When
        $actual = $shipping_method_display_nps_decision_under_tests->should_display_nps();

        // Then
        $this->assertFalse( $actual );
    }

    public function testShouldReturnTrueWhenOnShippingMethodSettingsButMethodIdMatch() {
        // Given
        $shipping_method = $this->getMockBuilder( '\WC_Shipping_Method' )->getMock();
        $shipping_method->id = 'flexible_shipping';
        $shipping_method_display_nps_decision_under_tests = $this->getMockBuilder( ShippingMethodDisplayNpsDecision::class )
                                                                 ->setMethods( array( 'get_shipping_method' ) )
                                                                 ->setConstructorArgs( array( 'flexible_shipping' ) )
                                                                 ->getMock();
        $shipping_method_display_nps_decision_under_tests->method( 'get_shipping_method' )->willReturn( $shipping_method );
        $_GET['page'] = 'wc-settings';
        $_GET['tab'] = 'shipping';
        $_GET['instance_id'] = 1;

        // When
        $actual = $shipping_method_display_nps_decision_under_tests->should_display_nps();

        // Then
        $this->assertTrue( $actual );
    }

}
