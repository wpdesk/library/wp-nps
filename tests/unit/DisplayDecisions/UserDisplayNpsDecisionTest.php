<?php

namespace unit\DisplayDecisions;

use WP_Mock\Tools\TestCase;
use WPDesk\Nps\DisplayDecisions\UserDisplayNpsDecision;

class UserDisplayNpsDecisionTest extends TestCase {

    public function setUp() {
        \WP_Mock::setUp();
        if ( ! defined( 'WEEK_IN_SECONDS' ) ) {
            define( 'WEEK_IN_SECONDS', 60 * 60 * 24 * 7 );
        }
    }

    public function tearDown() {
        \WP_Mock::tearDown();
    }

    public function testShouldReturnTrueWhenNoUserMeta() {
        // Expects
        $user = new \stdClass();
        $user->ID = 100;
        \WP_Mock::userFunction( 'wp_get_current_user' )->once()->andReturn( $user );
        \WP_Mock::userFunction( 'get_user_meta' )->once()->andReturn( array() );

        // Given
        $user_display_nps_decision_under_test = new UserDisplayNpsDecision( 'nps_test' );

        // When
        $actual = $user_display_nps_decision_under_test->should_display_nps();

        // Then
        $this->assertTrue( $actual );
    }

    public function testShouldReturnFalseWhenSent() {
        // Expects
        $user = new \stdClass();
        $user->ID = 100;
        \WP_Mock::userFunction( 'wp_get_current_user' )->once()->andReturn( $user );
        \WP_Mock::userFunction( 'get_user_meta' )->once()->andReturn( array( 'sent' => 'yes' ) );

        // Given
        $user_display_nps_decision_under_test = new UserDisplayNpsDecision( 'nps_test' );

        // When
        $actual = $user_display_nps_decision_under_test->should_display_nps();

        // Then
        $this->assertFalse( $actual );
    }

    public function testShouldReturnTrueWhenDismissCountBelowMaxSent() {
        // Expects
        $user = new \stdClass();
        $user->ID = 100;
        \WP_Mock::userFunction( 'wp_get_current_user' )->once()->andReturn( $user );
        \WP_Mock::userFunction( 'get_user_meta' )->once()->andReturn( array( 'dismiss_count' => 1 ) );

        // Given
        $user_display_nps_decision_under_test = new UserDisplayNpsDecision( 'nps_test' );

        // When
        $actual = $user_display_nps_decision_under_test->should_display_nps();

        // Then
        $this->assertTrue( $actual );
    }

    public function testShouldReturnFalseWhenDismissCountBelowMaxSentAndDismissTimeNotMatch() {
        // Expects
        $user = new \stdClass();
        $user->ID = 100;
        \WP_Mock::userFunction( 'wp_get_current_user' )->once()->andReturn( $user );
        \WP_Mock::userFunction( 'current_time' )->once()->andReturn( time() );
        \WP_Mock::userFunction( 'get_user_meta' )->once()->andReturn( array( 'dismiss_count' => 1, 'dismiss_time' => time() ) );

        // Given
        $user_display_nps_decision_under_test = new UserDisplayNpsDecision( 'nps_test' );

        // When
        $actual = $user_display_nps_decision_under_test->should_display_nps();

        // Then
        $this->assertFalse( $actual );
    }

    public function testShouldReturnTrueWhenDismissCountBelowMaxSentAndDismissTimeMatch() {
        // Expects
        $user = new \stdClass();
        $user->ID = 100;
        \WP_Mock::userFunction( 'wp_get_current_user' )->once()->andReturn( $user );
        \WP_Mock::userFunction( 'current_time' )->once()->andReturn( time() );
        \WP_Mock::userFunction( 'get_user_meta' )->once()->andReturn( array( 'dismiss_count' => 1, 'dismiss_time' => time() - WEEK_IN_SECONDS - 1 ) );

        // Given
        $user_display_nps_decision_under_test = new UserDisplayNpsDecision( 'nps_test' );

        // When
        $actual = $user_display_nps_decision_under_test->should_display_nps();

        // Then
        $this->assertTrue( $actual );
    }

    public function testShouldReturnFalseWhenDismissCountAboveMaxSent() {
        // Expects
        $user = new \stdClass();
        $user->ID = 100;
        \WP_Mock::userFunction( 'wp_get_current_user' )->once()->andReturn( $user );
        \WP_Mock::userFunction( 'get_user_meta' )->once()->andReturn( array( 'dismiss_count' => 2, 'dismiss_time' => time() - WEEK_IN_SECONDS - 1 ) );

        // Given
        $user_display_nps_decision_under_test = new UserDisplayNpsDecision( 'nps_test' );

        // When
        $actual = $user_display_nps_decision_under_test->should_display_nps();

        // Then
        $this->assertFalse( $actual );
    }

}
