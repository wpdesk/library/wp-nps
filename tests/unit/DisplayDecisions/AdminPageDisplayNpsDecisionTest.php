<?php

namespace unit\DisplayDecisions;

use WP_Mock\Tools\TestCase;
use WPDesk\Nps\DisplayDecisions\AdminPageDisplayNpsDecision;

class AdminPageDisplayNpsDecisionTest extends TestCase {

    public function setUp() {
        \WP_Mock::setUp();
    }

    public function tearDown() {
        \WP_Mock::tearDown();
    }

    public function testShouldReturnTrueWhenNoParametersPassed() {
        // Given
        $admin_page_display_nps_decision_under_tests = new AdminPageDisplayNpsDecision( array() );

        // When
        $actual = $admin_page_display_nps_decision_under_tests->should_display_nps();

        // Then
        $this->assertTrue( $actual );
    }

    public function testShouldReturnFalseWhenSingleParameterNotMatch() {
        // Given
        $admin_page_display_nps_decision_under_tests = new AdminPageDisplayNpsDecision( array( 'page' => 'setting' ) );
        $_GET['page'] = 'other';

        // When
        $actual = $admin_page_display_nps_decision_under_tests->should_display_nps();

        // Then
        $this->assertFalse( $actual );
    }

    public function testShouldReturnTrueWhenSingleParameterMatch() {
        // Given
        $admin_page_display_nps_decision_under_tests = new AdminPageDisplayNpsDecision( array( 'page' => 'settings' ) );
        $_GET['page'] = 'settings';

        // When
        $actual = $admin_page_display_nps_decision_under_tests->should_display_nps();

        // Then
        $this->assertTrue( $actual );
    }

    public function testShouldReturnFalseWheOnlySingleParameterMatch() {
        // Given
        $admin_page_display_nps_decision_under_tests = new AdminPageDisplayNpsDecision( array( 'page' => 'settings', 'tab' => 'shipping' ) );
        $_GET['page'] = 'settings';
        $_GET['tab'] = 'payment';

        // When
        $actual = $admin_page_display_nps_decision_under_tests->should_display_nps();

        // Then
        $this->assertFalse( $actual );
    }

    public function testShouldReturnTrueWheAllParametersMatch() {
        // Given
        $admin_page_display_nps_decision_under_tests = new AdminPageDisplayNpsDecision( array( 'page' => 'settings', 'tab' => 'shipping' ) );
        $_GET['page'] = 'settings';
        $_GET['tab'] = 'shipping';

        // When
        $actual = $admin_page_display_nps_decision_under_tests->should_display_nps();

        // Then
        $this->assertTrue( $actual );
    }

}
