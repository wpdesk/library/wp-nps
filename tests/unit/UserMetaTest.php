<?php

namespace unit;

use WP_Mock\Tools\TestCase;
use WPDesk\Nps\UserMeta;

class UserMetaTest extends TestCase {

    public function setUp() {
        \WP_Mock::setUp();
    }

    public function tearDown() {
        \WP_Mock::tearDown();
    }

    public function testShouldReturnEmptyArrayWhenThereNoMeta() {
        // Expects
        \WP_Mock::userFunction( 'get_user_meta' )->andReturn( false );

        // Given
        $current_user = new \stdClass();
        $current_user->ID = 100;
        $user_meta_under_tests = new UserMeta( $current_user, 'user_meta_name' );

        // When
        $actual = $user_meta_under_tests->get_meta_value();

        //Then
        $this->assertEquals( array(), $actual );
    }

    public function testShouldReturnArrayWhenThereIsMeta() {
        // Expects
        $meta_value = array( 'dismiss_count' => 2 );
        \WP_Mock::userFunction( 'get_user_meta' )->andReturn( $meta_value );

        // Given
        $current_user = new \stdClass();
        $current_user->ID = 100;
        $user_meta_under_tests = new UserMeta( $current_user, 'user_meta_name' );

        // When
        $actual = $user_meta_under_tests->get_meta_value();

        //Then
        $this->assertEquals( $meta_value, $actual );
    }

    public function testShouldReturnMetaValue() {
        // Expects
        $meta_value = array( 'dismiss_count' => 2 );
        \WP_Mock::userFunction( 'get_user_meta' )->andReturn( $meta_value );

        // Given
        $current_user = new \stdClass();
        $current_user->ID = 100;
        $user_meta_under_tests = new UserMeta( $current_user, 'user_meta_name' );

        // When
        $actual = $user_meta_under_tests->get_from_meta_value( 'dismiss_count', 0 );

        //Then
        $this->assertEquals( 2, $actual );
    }

    public function testShouldReturnDefaultWhenNoMetaValue() {
        // Expects
        $meta_value = array( 'dismiss_count' => 2 );
        \WP_Mock::userFunction( 'get_user_meta' )->andReturn( $meta_value );

        // Given
        $current_user = new \stdClass();
        $current_user->ID = 100;
        $user_meta_under_tests = new UserMeta( $current_user, 'user_meta_name' );

        // When
        $actual = $user_meta_under_tests->get_from_meta_value( 'dismiss_time', 0 );

        //Then
        $this->assertEquals( 0, $actual );
    }

    public function testShouldUpdateDismissMeta() {
        // Expects
        $meta_value = array( 'dismiss_count' => 1 );
        \WP_Mock::userFunction( 'get_user_meta' )->andReturn( $meta_value );
        \WP_Mock::userFunction( 'current_time' )->andReturn( 2 );
        \WP_Mock::userFunction( 'update_user_meta' )->once()->withArgs( array( 100, 'user_meta_name', array( 'dismiss_count' => 2, 'dismiss_time' => 2 ) ) );

        // Given
        $current_user = new \stdClass();
        $current_user->ID = 100;
        $user_meta_under_tests = new UserMeta( $current_user, 'user_meta_name' );

        // When
        $user_meta_under_tests->update_dismiss_meta();

        //Then
        $this->assertTrue( true );
    }

    public function testShouldUpdateSentMeta() {
        // Expects
        $meta_value = array( 'dismiss_count' => 1 );
        \WP_Mock::userFunction( 'get_user_meta' )->andReturn( $meta_value );
        \WP_Mock::userFunction( 'current_time' )->andReturn( 2 );
        \WP_Mock::userFunction( 'update_user_meta' )->once()->withArgs( array( 100, 'user_meta_name', array( 'dismiss_count' => 1, 'sent' => 'yes' ) ) );

        // Given
        $current_user = new \stdClass();
        $current_user->ID = 100;
        $user_meta_under_tests = new UserMeta( $current_user, 'user_meta_name' );

        // When
        $user_meta_under_tests->update_sent_meta();

        //Then
        $this->assertTrue( true );
    }

}
