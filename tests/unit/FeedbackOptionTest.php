<?php

namespace unit;

use WP_Mock\Tools\TestCase;
use WPDesk\Nps\AjaxHandler;
use WPDesk\Nps\FeedbackOption;

class FeedbackOptionTest extends TestCase {

    /**
     * @var FeedbackOption
     */
    private $feedback_option;

    public function setUp() {
        \WP_Mock::setUp();

        $this->feedback_option = new FeedbackOption( 'test', "question" );
    }

    public function tearDown() {
        \WP_Mock::tearDown();
    }

    public function testShouldReturnText() {
        // Then
        $this->assertEquals( 'test', $this->feedback_option->get_label() );
    }

    public function testShouldReturnAdditionalQuestion() {
        // Then
        $this->assertEquals( 'question', $this->feedback_option->get_additional_question() );
    }

}
