<?php

namespace unit;

use WP_Mock\Tools\TestCase;
use WPDesk\Nps\UuidUserId;

class UuidUserIdTest extends TestCase {

    public function setUp() {
        \WP_Mock::setUp();
    }

    public function tearDown() {
        \WP_Mock::tearDown();
    }

    public function testShouldReturnNewIdWhenNotPresent() {
        // Expects
        $id = 'a-b-c-d';
        $user = new \stdClass();
        $user->ID = 100;
        \WP_Mock::userFunction( 'wp_get_current_user' )->andReturn( $user );
        \WP_Mock::userFunction( 'get_user_meta' )->andReturn( false );
        \WP_Mock::userFunction( 'add_user_meta' )->withArgs( array( 100, 'user_meta_name', $id, true ) )->andReturn( true );
        \WP_Mock::userFunction( 'wp_generate_uuid4' )->andReturn( $id );

        // Given
        $current_user = new \stdClass();
        $current_user->ID = 100;
        $user_meta_under_tests = new UuidUserId( 'user_meta_name' );

        // When
        $actual = $user_meta_under_tests->get_id();

        //Then
        $this->assertEquals( $id, $actual );
    }

    public function testShouldReturnActualIdWhenPresent() {
        // Expects
        $id = 'a-b-c-d';
        $user = new \stdClass();
        $user->ID = 100;
        \WP_Mock::userFunction( 'wp_get_current_user' )->andReturn( $user );
        \WP_Mock::userFunction( 'get_user_meta' )->andReturn( $id );
        \WP_Mock::userFunction( 'get_user_meta' )->andReturn( $id );
        \WP_Mock::userFunction( 'add_user_meta' )->never();
        \WP_Mock::userFunction( 'wp_is_uuid' )->once()->andReturn( true );
        \WP_Mock::userFunction( 'wp_generate_uuid4' )->never();

        // Given
        $current_user = new \stdClass();
        $current_user->ID = 100;
        $user_meta_under_tests = new UuidUserId( 'user_meta_name' );

        // When
        $actual = $user_meta_under_tests->get_id();

        //Then
        $this->assertEquals( $id, $actual );
    }

    public function testShouldReturnAnonymousWhenWPGetCurrentUserReturnsFalse() {
        // Expects
        \WP_Mock::userFunction( 'wp_get_current_user' )->andReturn( false );

        // Given
        $current_user = new \stdClass();
        $current_user->ID = 100;
        $user_meta_under_tests = new UuidUserId( 'user_meta_name' );

        // When
        $actual = $user_meta_under_tests->get_id();

        //Then
        $this->assertEquals( 'Anonymous', $actual );
    }

}
