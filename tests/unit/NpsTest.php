<?php

namespace unit;

use WP_Mock\Tools\TestCase;
use WPDesk\Nps\DisplayDecisions\DisplayNpsDecision;
use WPDesk\Nps\Element;
use WPDesk\Nps\FeedbackOption;
use WPDesk\Nps\Nps;

class NpsTest extends TestCase {

    /**
     * @var \PHPUnit\Framework\MockObject\MockObject|DisplayNpsDecision
     */
    private $display_nps_decisions;
    /**
     * @var Element
     */
    private $nps_under_tests;

    public function setUp() {
        \WP_Mock::setUp();

        $this->display_nps_decisions = $this->getMockBuilder( DisplayNpsDecision::class )->setMethods( array( 'should_display_nps' ) )->getMock();
        $this->nps_under_tests = $this->getMockBuilder( Nps::class )
                                      ->setMethods( array( 'add_hookable' ) )
                                      ->setConstructorArgs( array( 'nps_id', 'satismeter_write_key', 'satismeter_campaign_id', 'satismeter_feedback_id', 'satismeter_user_id', 'assets_url', 'scrips_version', $this->display_nps_decisions, 'user_meta_name', 'ajax_url' ) )
                                      ->getMock();
    }

    public function tearDown() {
        \WP_Mock::tearDown();
    }

    public function testShouldEnqueueScripts() {
        // Expects
        $this->nps_under_tests->expects( $this->exactly( 3 ) )->method( 'add_hookable' );

        // When
        $this->nps_under_tests->hooks();
        do_action( 'admin_enqueue_scripts' );

        // Then
        $this->assertTrue( true );
    }

    public function testSetterShouldReturnNps() {
        // When
        $actual = $this->nps_under_tests->set_best_label( '' )
                                        ->set_disclaimer( '' )
                                        ->set_element_id( '' )
                                        ->set_question( '' )
                                        ->set_send_button( '' )
                                        ->set_thank_you_message( '' )
                                        ->set_send_button_sending( '' )
                                        ->set_worst_label( '' )
                                        ->set_feedback_question( '' )
                                        ->clear_feedback_options()
                                        ->add_feedback_option( new FeedbackOption( 'option', true ) );

        // Then
        $this->assertInstanceOf( Nps::class, $actual );
    }

}
