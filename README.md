[![Latest Stable Version](https://poser.pugx.org/wpdesk/wp-nps/v/stable)](https://packagist.org/packages/wpdesk/library/wp-nps)
[![Total Downloads](https://poser.pugx.org/wpdesk/wp-nps/downloads)](https://packagist.org/packages/wpdesk/library/wp-nps)
[![License](https://poser.pugx.org/wpdesk/wp-nps/license)](https://packagist.org/packages/wpdesk/library/wp-nps)

# NPS

A WordPress library containing interfaces, abstracts and basic implementations to display the plugins-dedicated Net Promoter Score survey form. 

## Requirements

PHP 7.0 or later.

## Installation via Composer

In order to install the bindings via [Composer](http://getcomposer.org/) run the following command:

```bash
composer reqiure wpdesk/wp-nps
```

## Example usage

Displaying the NPS on [Flexible Shipping](https://octolize.com/product/flexible-shipping-pro-woocommerce/) shipping method's settings screen:

```php
$user_meta_name = 'fs_nps';
$nps            = new \WPDesk\Nps\Nps(
    'fs_nps',
    'satismeter_write_key_here',
    'satismeter_campaign_id_here',
    'satismeter_feedback_id',
    new \WPDesk\Nps\UuidUserId(),
    $this->get_plugin_assets_url() . '../vendor_prefixed/wpdesk/wp-nps/assets/',
    $this->scripts_version,
    new \WPDesk\Nps\DisplayDecisions\DisplayNpsLocationsAndUserDecisions(
        [
            new \WPDesk\Nps\DisplayDecisions\ShippingMethodDisplayNpsDecision( 'flexible_shipping_single' ),
            new \WPDesk\Nps\DisplayDecisions\AdminPageDisplayNpsDecision( [
                'page'    => 'wc-settings',
                'tab'     => 'shipping',
                'section' => 'flexible_shipping_info',
            ] ),
        ],
        [
            new \WPDesk\Nps\DisplayDecisions\ShippingMethodDisplayNpsDecision( new \WPDesk\Nps\DisplayDecisions\UserDisplayNpsDecision( $user_meta_name ) ),
        ]
    ),
    $user_meta_name,
    admin_url( 'admin-ajax.php' )
);
$nps->set_question( __( 'Hey, we are curious how would you grade your first impression on Flexible Shipping so far?', 'flexible-shipping' ) );
$nps->set_best_label( __( 'Wow, it\'s awesome!', 'flexible-shipping' ) );
$nps->set_worst_label( __( 'Really poor', 'flexible-shipping' ) );
$nps->set_disclaimer(
    sprintf(
        __( 'By using the \'Send feedback\' button I hereby agree and consent to the terms of %1$sPrivacy Policy%2$s.', 'flexible-shipping' ),
        '<a target="_blank" href="https://octolize.com/terms-of-service/privacy-policy/">',
        '</a>'
    )
);

$nps->add_feedback_option( new FeedbackOption( __( 'More calculation conditions', 'flexible-shipping' ), __( 'What exactly should the conditions you need be based on?', 'flexible-shipping' ) ) )
    ->add_feedback_option( new FeedbackOption( __( 'More detailed documentation', 'flexible-shipping' ) ) )
    ->add_feedback_option( new FeedbackOption( __( 'Easier contact with support', 'flexible-shipping' ) ) )
    ->add_feedback_option( new FeedbackOption( __( 'More user-friendly interface', 'flexible-shipping' ) ) )
    ->add_feedback_option( new FeedbackOption( __( 'Less complicated settings', 'flexible-shipping' ) ) )
    ->add_feedback_option( new FeedbackOption( __( 'Too soon to say', 'flexible-shipping' ) ) )
    ->add_feedback_option( new FeedbackOption( __( 'Other', 'flexible-shipping' ), __( 'Please tell us what exactly can we do to improve our plugin?', 'flexible-shipping' ) ) );

$nps->hooks();
```

## Developers

Please remember to execute: `npm run prod` before commit!
